/* Timer.h, (Created on 30.03.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SAYONARA_PLAYER_TIMER_H
#define SAYONARA_PLAYER_TIMER_H

#include "Pimpl.h"

#include <chrono>
#include <functional>

namespace Utils
{
	class Timer
	{
		PIMPL(Timer)

		public:
			Timer();
			~Timer() noexcept;

			Timer(const Timer& other) = delete;
			Timer(Timer&& other) = delete;
			Timer& operator=(const Timer& other) = delete;
			Timer& operator=(Timer&& other) = delete;

			using Callback = std::function<bool()>;

			void start(std::chrono::milliseconds interval, const int iterations, Callback&& callback);
			void stop();
			[[nodiscard]] bool isActive() const;
	};

} // Utils

#endif //SAYONARA_PLAYER_TIMER_H
