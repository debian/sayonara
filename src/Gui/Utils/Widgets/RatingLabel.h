/* RatingLabel.h */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RATINGLABEL_H
#define RATINGLABEL_H

#include "Utils/Pimpl.h"

#include <QLabel>
#include <QPoint>
#include <QSize>

namespace Gui
{
	class RatingLabel :
		public QLabel
	{
		Q_OBJECT
		PIMPL(RatingLabel)

		public:
			enum class Alignment :
				uint8_t
			{
				Left,
				Centered
			};

			explicit RatingLabel(QWidget* parent, bool enabled = true);
			~RatingLabel() override;

			void setRating(Rating rating);
			[[nodiscard]] Rating rating() const;
			[[nodiscard]] Rating ratingAt(QPoint pos) const;

			void setVerticalOffset(int offset);

			void setHorizontalAlignment(Alignment alignment);

			void paint(QPainter* painter, const QRect& rect);

			[[nodiscard]] QSize sizeHint() const override;

			[[nodiscard]] QSize minimumSizeHint() const override;

		private:
			using QLabel::setAlignment;
	};

	class RatingEditor :
		public QWidget
	{
		Q_OBJECT
		PIMPL(RatingEditor)

		signals:
			void sigFinished(bool save);

		public:
			explicit RatingEditor(QWidget* parent);
			RatingEditor(Rating rating, QWidget* parent);
			~RatingEditor() override;

			void setRating(Rating rating);

			[[nodiscard]] Rating rating() const;

			void setVerticalOffset(int offset);

			void setHorizontalAlignment(RatingLabel::Alignment alignment);

			void setMouseTrackable(bool b);

			[[nodiscard]] QSize sizeHint() const override;

			[[nodiscard]] QSize minimumSizeHint() const override;

		protected:
			void paintEvent(QPaintEvent* e) override;

			void focusInEvent(QFocusEvent* e) override;
			void focusOutEvent(QFocusEvent* e) override;

			void mousePressEvent(QMouseEvent* e) override;
			void mouseMoveEvent(QMouseEvent* e) override;
			void mouseReleaseEvent(QMouseEvent* e) override;
	};
}

#endif // RATINGLABEL_H
