/* LyricLookup.h */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * LyricLookup.h
 *
 *  Created on: May 21, 2011
 *      Author: Michael Lugmair (Lucio Carreras)
 */

#ifndef SAYONARA_PLAYER_LYRIC_LOOKUP_H
#define SAYONARA_PLAYER_LYRIC_LOOKUP_H

#include <QObject>

#include "Utils/Pimpl.h"

namespace Lyrics
{
	class Server;
	class Lookup :
		public QObject
	{
		Q_OBJECT
		PIMPL(Lookup)

		signals:
			void sigFinished();

		public:
			explicit Lookup(QObject* parent = nullptr);
			~Lookup() override;

			[[nodiscard]] QString lyricData() const;
			[[nodiscard]] QString lyricHeader() const;
			[[nodiscard]] bool hasError() const;

			void run(const QString& artist, const QString& title, Server* server);
			void stop();

		private:
			void startSearch(const QString& url, Server* server);
			void callWebsite(const QString& url, Server* server);

		private slots: // NOLINT(*-redundant-access-specifiers)
			void contentFetched();
			void searchFinished();
	};
}

#endif /* SAYONARA_PLAYER_LYRIC_LOOKUP_H */
