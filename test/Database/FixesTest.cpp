/*
 * Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common/SayonaraTest.h"
#include "Database/Connector.h"
#include "Database/Module.h"
#include "Database/Fixes.h"

#include "Database/Query.h"

// access working directory with Test::Base::tempPath("somefile.txt");

namespace
{
	class TestDatabaseFixes :
		public DB::Fixes
	{
		public:
			TestDatabaseFixes(const QString& connectionName, const DbId dbId) :
				DB::Fixes::Fixes(connectionName, dbId) {}

			~TestDatabaseFixes() noexcept override = default;

			void applyFixes() override {}
	};
}

class FixesTest :
	public Test::Base
{
	Q_OBJECT

	public:
		FixesTest() :
			Test::Base("FixesTest"),
			m_connectionName {DB::Connector::instance()->connectionName()},
			m_dbId {DB::Connector::instance()->databaseId()} {}

	private:
		QString m_connectionName;
		DbId m_dbId;

		QStringList getAllViews()
		{
			auto module = DB::Module(m_connectionName, m_dbId);
			auto q = module.runQuery("SELECT name FROM sqlite_master WHERE type='view';", "Cannot get all views");
			auto views = QStringList {};
			while(q.next())
			{
				views << q.value(0).toString();
			}

			return views;
		}

		QStringList getAllTables()
		{
			auto module = DB::Module(m_connectionName, m_dbId);
			auto q = module.runQuery("SELECT name FROM sqlite_master WHERE type='table';", "Cannot get all tables");
			auto tables = QStringList {};
			while(q.next())
			{
				tables << q.value(0).toString();
			}

			return tables;
		}

	private slots: // NOLINT(*-redundant-access-specifiers)
		// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
		[[maybe_unused]] void testIfTableExists()
		{
			auto fixes = TestDatabaseFixes(m_connectionName, m_dbId);

			struct TestCase
			{
				QString tablename;
				bool expectedResult;
			};

			const auto testCases = std::array {
				TestCase {"Tracks", true},
				TestCase {"TrAcKs", true},
				TestCase {"asdfasdfasd", false}
			};

			for(const auto& testCase: testCases)
			{
				QCOMPARE(fixes.checkIfTableExists(testCase.tablename), testCase.expectedResult);
			}
		}

		// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
		[[maybe_unused]] void testDropAllViews()
		{
			auto views = getAllViews();
			auto tables = getAllTables();
			tables.sort();
			QVERIFY(!views.isEmpty());
			QVERIFY(!tables.isEmpty());

			auto fixes = TestDatabaseFixes(m_connectionName, m_dbId);
			fixes.dropAllViews();

			auto currentViews = getAllViews();
			auto currentTables = getAllTables();
			currentTables.sort();
			QVERIFY(currentViews.isEmpty());
			QCOMPARE(tables, currentTables);
		}
};

QTEST_GUILESS_MAIN(FixesTest)

#include "FixesTest.moc"
