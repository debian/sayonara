/*
 * Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common/SayonaraTest.h"
#include "Common/FileSystemMock.h"
#include "Common/TaggingMocks.h"

#include "Utils/Parser/M3UParser.h"
#include "Utils/MetaData/MetaDataList.h"

#include <QMap>
#include <QStringList>

// access working directory with Test::Base::tempPath("somefile.txt");


class M3UParserTest :
	public Test::Base
{
	Q_OBJECT

	public:
		M3UParserTest() :
			Test::Base("M3UParserTest") {}

	private slots:

		// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
		[[maybe_unused]] void testParseM3UFile()
		{
			constexpr const auto* playlistFile = "/path/to/playlist/playlist.m3u";
			constexpr const auto* url = "http://dispatcher.rndfnk.com/rbb/inforadio/live/mp3/low";
			const auto fileSystem = std::make_shared<Test::FileSystemMock>(
				QMap<QString, QStringList> {
					{"/path/to/playlist", {"playlist.m3u"}},
				});

			const auto data = QStringList {
				"#EXTM3U",
				QString {},
				"#EXTINF:-1,Inforadio vom rbb live hören - www.inforadio.de",
				url
			}.join("\n");

			fileSystem->writeFile(data.toLocal8Bit(), playlistFile);

			auto parser = M3UParser(playlistFile, fileSystem, std::make_shared<Test::TagReaderMock>());
			const auto tracks = parser.tracks();

			QCOMPARE(tracks.count(), 1);
			QCOMPARE(tracks[0].filepath(), url);
			QCOMPARE(tracks[0].title(), "www.inforadio.de");
			QCOMPARE(tracks[0].artist(), "Inforadio vom rbb live hören");
			QCOMPARE(tracks[0].durationMs(), -1000);
		}
};

QTEST_GUILESS_MAIN(M3UParserTest)

#include "M3UParserTest.moc"
